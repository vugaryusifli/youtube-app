import * as React from 'react';
import {BottomNavigation} from 'react-native-paper';
import { StyleSheet, Text } from 'react-native';
import Home from "./Home/Home";

const Explore = () => <Text>Explore</Text>;
const Subscriptions = () => <Text>Subscriptions</Text>;
const Notifications = () => <Text>Notifications</Text>;
const Library = () => <Text>Library</Text>;

const BottomMenu = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'home', title: 'Home', icon: 'home' },
        { key: 'explore', title: 'Explore', icon: 'compass' },
        { key: 'subscriptions', title: 'Subscriptions', icon: 'youtube-subscription', badge: true },
        { key: 'notifications', title: 'Notifications', icon: 'bell' },
        { key: 'library', title: 'Library', icon: 'clipboard-play' }
    ]);

    const renderScene = BottomNavigation.SceneMap({
        home: Home,
        explore: Explore,
        subscriptions: Subscriptions,
        notifications: Notifications,
        library: Library
    });

    return (
        <BottomNavigation
            barStyle={styles.nav}
            navigationState={{ index, routes }}
            onIndexChange={setIndex}
            renderScene={renderScene}
            activeColor="red"
            shifting={false}
            getLabelText={({ route }) => <Text style={styles.text}>{route.title}</Text>}
        />
    );
};

const styles = StyleSheet.create({
    nav: {
        backgroundColor: "#fff"
    },
    text: {
        fontSize: 10
    }
});

export default BottomMenu;
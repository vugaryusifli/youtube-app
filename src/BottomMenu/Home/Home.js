import React from "react";
import {Dimensions, FlatList, SafeAreaView, StyleSheet, View} from "react-native";
import {Avatar, Card, Chip, IconButton, Menu} from "react-native-paper";
import {useDispatch} from "react-redux";
import {getData} from "../../../redux/actions/actions";

const Home = () => {
    const dispatch = useDispatch();
    const [data, setData] = React.useState([
        {id: '1', title: 'All', open: false},
        {id: '2', title: 'Military People', open: false},
        {id: '3', title: 'Airplanes', open: false},
        {id: '4', title: 'JavaScript', open: false},
        {id: '5', title: 'Rapping', open: false}
    ]);

    React.useEffect(() => {
        dispatch(getData());
    }, [dispatch]);

    const openMenu = (id) => {
        const checkId = data.map(d => d.id === id ? {...d, open: !d.open} : d);
        setData(checkId);
    };

    const closeMenu = (id) => {
        const checkId = data.map(d => d.id === id ? {...d, open: false} : d);
        setData(checkId);
    };

    const renderItem = ({item}) => {
        return (
            <View style={{
                paddingHorizontal: 5,
                paddingVertical: 6,
                paddingBottom: 10
            }}>
                <Chip key={item.id} onPress={() => console.log('Pressed')}>{item.title}</Chip>
            </View>
        )
    };

    const renderItem2 = ({ item }) => {
        return (
            <Card style={{
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0
            }}>
                <Card.Cover source={{ uri: 'https://i.pinimg.com/originals/a3/8e/66/a38e66e215b90a6edc273189798777ad.jpg' }} />
                <Card.Title
                    title={item.title}
                    subtitle="Card Subtitle * 14K views * 3 hours ago"
                    left={(props) => <Avatar.Image {...props} source={{
                        uri: 'https://lh3.googleusercontent.com/a-/AOh14GgT3N1y1Lwfkx7CUb7NMdxEm_7URWu_19TLH6hdvQ=s88-c-k-c0x00ffffff-no-rj-mo'
                    }} />}
                    right={(props) => {
                        return (
                            <View>
                                <Menu
                                    visible={item.open}
                                    onDismiss={() => closeMenu(item.id)}
                                    anchor={<IconButton {...props} icon="dots-vertical" onPress={() => openMenu(item.id)} />}>
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Save to Watch later" />
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Save to playlist" />
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Share" />
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Not interested" />
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Don't recommend channel" />
                                    <Menu.Item onPress={() => closeMenu(item.id)} title="Report" />
                                </Menu>
                            </View>
                        )
                    }}
                />
            </Card>
        )
    };


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <FlatList
                style={{ marginHorizontal: 5 }}
                horizontal
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                showsHorizontalScrollIndicator={false}
            />
            <FlatList
                data={data}
                renderItem={renderItem2}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
            />
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({

});

export default Home;
import React from 'react';
import {StyleSheet, View, TouchableOpacity} from "react-native";
import {Appbar, Dialog, List, Portal, Avatar} from 'react-native-paper';
import {SearchBar} from "react-native-elements";
import {Image} from "react-native";
// import logo from "../../assets/youtube_logo.svg";

const Navbar = ({
    showSearchBar,
    handleHide,
    handleShow,
    searchQuery,
    openVideoDialog,
    hideVideoDialog,
    showVideoDialog,
    onChangeSearch
}) => {
    return (
        <View>
            <Appbar.Header style={styles.header}>
                {
                    !showSearchBar && (
                        <Appbar.Content title={
                            <View>
                                <Image style={styles.image} source={{
                                    uri: 'https://themrphone.com/tech/wp-content/uploads/2017/10/youtubelogo.png'
                                }} />
                            </View>
                        } />
                    )
                }
                {
                    showSearchBar && <Appbar.Action icon="arrow-left" onPress={handleHide}/>
                }
                {
                    showSearchBar && (
                        <View style={{flex: 1, paddingHorizontal: 6}}>
                            <SearchBar
                                value={searchQuery}
                                placeholder="Search YouTube"
                                searchIcon={null}
                                clearIcon={{
                                    color: '#666666',
                                    iconStyle: {
                                        fontSize: 22
                                    }
                                }}
                                rightIconContainerStyle={{
                                    paddingRight: 0,
                                    marginRight: 8,
                                    marginLeft: 8
                                }}
                                placeholderTextColor="#666666"
                                containerStyle={{
                                    backgroundColor: 'transparent',
                                    padding: 0,
                                    borderBottomWidth: 0,
                                    borderTopWidth: 0
                                }}
                                inputContainerStyle={{
                                    backgroundColor: '#ededed',
                                    height: 34
                                }}
                                inputStyle={{
                                    color: '#000000',
                                    fontSize: 18,
                                }}
                                autoFocus
                                onChangeText={onChangeSearch}
                            />
                        </View>
                    )
                }
                {
                    showSearchBar && searchQuery.length === 0 && <Appbar.Action icon="microphone" onPress={() => {}}/>
                }
                {
                    !showSearchBar && <Appbar.Action icon="video" onPress={showVideoDialog}/>
                }
                {
                    !showSearchBar && <Appbar.Action icon="magnify" onPress={handleShow}/>
                }
                {
                    !showSearchBar && (
                        <TouchableOpacity onPress={() => console.log(2)}>
                            <Avatar.Image style={{ marginHorizontal: 13 }} size={24} source={{
                                uri: 'https://lh3.googleusercontent.com/a-/AOh14GgT3N1y1Lwfkx7CUb7NMdxEm_7URWu_19TLH6hdvQ=s88-c-k-c0x00ffffff-no-rj-mo'
                            }} />
                        </TouchableOpacity>
                    )
                }
            </Appbar.Header>

            <View>
                {/*<Animated.View style={[{*/}
                {/*    transform: [{translateY: modalY}]*/}
                {/*}]}>*/}
                    <Portal>
                        <Dialog
                            style={{
                                position: 'absolute',
                                bottom: 0,
                                left: 0,
                                right: 0,
                                marginBottom: 0,
                                marginHorizontal: 0,
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                                borderBottomLeftRadius: 0,
                                borderBottomRightRadius: 0,
                            }}
                            visible={openVideoDialog}
                            onDismiss={hideVideoDialog}
                        >
                            <Dialog.Title style={{ marginHorizontal: 20 }}>
                                Create
                            </Dialog.Title>
                            <Dialog.Content style={{ paddingHorizontal: 0, paddingBottom: 0 }}>
                                <List.Item
                                    onPress={() => {}}
                                    title="Upload a video"
                                    left={props => <List.Icon {...props} style={{ backgroundColor: '#dbdbdb', borderRadius: 50 }} icon="upload" />}
                                />
                                <List.Item
                                    onPress={() => {}}
                                    title="Go live"
                                    left={props => <List.Icon {...props} style={{ backgroundColor: '#dbdbdb', borderRadius: 50 }} icon="access-point" />}
                                />
                            </Dialog.Content>
                        </Dialog>
                    </Portal>
                {/*</Animated.View>*/}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#fff'
    },
    image: {
        width: 87,
        height: 67
    }
});

export default Navbar;
import React from 'react';
import Navbar from "./Navbar/Navbar";
import Menu from "./BottomMenu/BottomMenu";
import {Text, View, FlatList, Dimensions} from "react-native";
import {List} from "react-native-paper";
import _ from 'lodash';

const Main = () => {
    const [showSearchBar, setShowSearchBar] = React.useState(false);
    const [searchQuery, setSearchQuery] = React.useState('');
    const [openVideoDialog, setOpenVideoDialog] = React.useState(false);
    const screenHeight = Dimensions.get('window').height;

    const onChangeSearch = query => {
        setSearchQuery(query);
    };

    const showVideoDialog = () => {
        setOpenVideoDialog(true);
    };

    const hideVideoDialog = () => {
        setOpenVideoDialog(false);
    };

    const handleShow = () => {
        setShowSearchBar(true);
    };

    const handleHide = () => {
        setSearchQuery('');
        setShowSearchBar(false);
    };

    const renderItem = () => {
        return (
            <List.Item
                onPress={() => {}}
                title="salam"
                left={props => <List.Icon {...props} icon="history" />}
                right={props => <List.Icon onPress={() => console.log(1)} {...props} icon="arrow-top-left" />}
            />
        )
    };

    return (
        <>
            <Navbar
                showSearchBar={showSearchBar}
                handleShow={handleShow}
                handleHide={handleHide}
                searchQuery={searchQuery}
                openVideoDialog={openVideoDialog}
                onChangeSearch={onChangeSearch}
                showVideoDialog={showVideoDialog}
                hideVideoDialog={hideVideoDialog}
            />
            {
                showSearchBar ? (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            keyExtractor={item => item.toString()}
                            data={_.times(50, Math.random)}
                            renderItem={renderItem}
                        />
                    </View>
                )
                    : <Menu />
            }
        </>
    );
};

export default Main;
import {configureStore} from "@reduxjs/toolkit";
import createReducer from './rootReducer';

const middlewares = [];

const StateInvariantMiddlewareOptions = {
    // serializableCheck: {
    // 	ignoredActions: ['dialog/openDialog', 'dialog/closeDialog', 'message/showMessage', 'message/hideMessage']
    // },
    serializableCheck: false,
    immutableCheck: false
};

const store = configureStore({
    reducer: createReducer(),
    middleware: getDefaultMiddleware => getDefaultMiddleware(StateInvariantMiddlewareOptions).concat(middlewares),
    devTools: process.env.NODE_ENV === 'development'
});

export default store;
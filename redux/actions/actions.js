import axios from 'axios';
import {createAction} from "@reduxjs/toolkit";

export const getData = () => async dispatch => {
    try {
        const res = await axios.get('https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&q=songs&type=video&key=AIzaSyBUul3LSmpIo-7enaC_Sf_Vde8HOGgUxQo');
        const data = await res.data;

        // console.log(data)
    } catch (e) {

    }
};

export const actionGetData = createAction('youtubeApp/actionGetData');


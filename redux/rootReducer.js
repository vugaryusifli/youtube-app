import {combineReducers} from '@reduxjs/toolkit';
import data from "./reducers/reducers";

const createReducer = asyncReducers =>
    combineReducers({
        data,
        ...asyncReducers
    });

export default createReducer;
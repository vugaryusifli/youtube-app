import {createReducer} from "@reduxjs/toolkit";
import {actionGetData} from "../actions/actions";

const initialState = {
    data: null
};

const reducer = createReducer(initialState, {
    [actionGetData.type]: state => state
});

export default reducer;
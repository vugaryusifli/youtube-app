import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';
import {Provider as StoreProvider} from 'react-redux';
import {expo} from './app.json';
import store from "./redux/store";
import Main from "./src/Main";

export default function App() {
    return (
        <StoreProvider store={store}>
            <PaperProvider>
               <Main/>
               <StatusBar />
            </PaperProvider>
        </StoreProvider>
    );
}

AppRegistry.registerComponent(expo.name, () => App);